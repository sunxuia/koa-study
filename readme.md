# 简单的koa 教程代码

参考了 [廖雪峰的教程](https://www.liaoxuefeng.com/wiki/001434446689867b27157e896e74d51a89c25cc8b43bdb3000/001434501579966ab03decb0dd246e1a6799dd653a15e1b000)

## c1  : 简单的示例

* 安装 koa
    > npm i -D koa

* 运行
    > npm run c1

* 浏览器

    [http://localhost:8080](http://localhost:8080)

## c2 : URL 路由

* 安装 koa-router
    > npm i -D koa-router

* 运行
    > npm run c2

* 浏览器

    [http://localhost:8080](http://localhost:8080)

    [http://localhost:8080/hello/me](http://localhost:8080/hello/me) hello/ 后面的字符串会显示在页面上

## c3 : 表单解析

* 安装 koa-bodyparser, 这个可以解析提交的请求里的数据.
    > npm i -D koa-bodyparser

* 运行
    > npm run c3

* 浏览器

    [http://localhost:8080/signin](http://localhost:8080/signin) 然后提交表单

## c4 : 模板处理(nunjucks)

* 安装 nunjucks, 一种模板引擎.
    > npm i -D nunjucks

* 文件

    视图模板是 nunjucks/hello.html

    模板引擎初始化的代码在 nunjucks/nunjucks.js

* 运行
    > npm run c4

* 浏览器

    [http://localhost:8080](http://localhost:8080)

## c5 : MVC

* 主页提供了登录选项, 登录如果密码如果是123 则显示成功的页面, 如果不是则显示不成功的页面.

* 文件

    视图模板在 views 目录中

    controller 在controllers 目录中

* 运行
    > npm run c5

* 浏览器

    [http://localhost:8080](http://localhost:8080)
