/**
 * 使用模板 ( nunjucks )
 */
const Koa = require('koa')
const nunjucks = require('./nunjucks/nunjucks')('nunjucks')

const app = new Koa()

// 返回一个渲染过的模板
app.use(async (ctx, next) => {
    // 模板渲染
    var html = nunjucks.render('hello.html', { 
        name : 'dude'
    })
    ctx.response.body = html
})


app.listen(8080)
console.log('koa server start at port 8080')


