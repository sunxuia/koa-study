module.exports = {
    url: '/signin',
    method : 'post',
    middleware: async (ctx, next) => {
        let name = ctx.request.body.name || ''
        let password = ctx.request.body.password || ''
        console.log(`sign in with name ${name}, password ${password}`)
        if (password === '123') {
            ctx.render('signin-success.html', {
                title: 'Sign In Success!',
                name
            })
        } else {
            ctx.render('signin-failure.html', {
                title: 'Sign In Failed!',
                name
            })
        }
    }
}