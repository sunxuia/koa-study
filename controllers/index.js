module.exports = {
    url: '/',
    middleware: async (ctx, next) => {
        ctx.render('index.html', {
            title: 'welcome (index)'
        })
    }
}