/**
 * 简单示例
 */
const Koa = require('koa')

const app = new Koa()

// 注册 middleware
app.use(async(ctx, next) => {
    await next(); // 调用下一个middle ware
    
    ctx.response.type = 'text/html'
    ctx.response.body = '<h1>hello world</h1>'
})

app.listen(8080)
console.log('koa server start at port 8080')