/**
 * URL 路由示例
 */
const Koa = require('koa')
// require 返回的是一个函数, 这个函数返回一个新的router
const router = require('koa-router')()

const app = new Koa()

// 日志处理
app.use(async (ctx, next) => {
    const start = new Date()
    try {
        console.log(`request ${ctx.method} ${ctx.request.url} from ${ctx.ip}`)
        await next();
    } catch (err) {
        console.log(`error : ${err}`)
    } finally {
        console.log(`request end, duration : ${new Date() - start} ms`)
    }
})

// 添加路由处理
// get 方法表示处理get 请求, 第一个参数是路径, 第二个参数是回调函数
router.get('/', async(ctx, next) => {
    ctx.response.body = `<h1>index</h1>`
})
router.get('/hello/:name', async(ctx, next) => {
    let name = ctx.params.name; //路径参数
    ctx.response.body = `<h1>hello, ${name}!</h1>`
})

// 添加路由
app.use(router.routes())

app.listen(8080)
console.log('koa server start at port 8080')


