/**
 * 提交表单数据的处理
 */
const Koa = require('koa')
const router = require('koa-router')()
// 用于解析请求中的参数
const bodyParser = require('koa-bodyparser')

const app = new Koa()

// 日志处理
app.use(async (ctx, next) => {
    const start = new Date()
    try {
        console.log(`request ${ctx.method} ${ctx.request.url} from ${ctx.ip}`)
        await next();
    } catch (err) {
        console.log(`error : ${err}`)
    } finally {
        console.log(`request end, duration : ${new Date() - start} ms`)
    }
})

// 添加路由处理
// 显示表单页
router.get('/signin', async(ctx, next) => {
    ctx.response.body = `<h1>Index</h1>
    <form action="/signin" method="post">
        <p>Name: <input name="name" value="koa"></p>
        <p>Password: <input name="password" type="password"></p>
        <p><input type="submit" value="Submit"></p>
    </form>`
})
// 处理表单提交
router.post('/signin', (ctx, next) => {
    // 获得提交的数据
    let name = ctx.request.body.name || ''
    let password = ctx.request.body.password || ''
    console.log(`sign in with name ${name}, password ${password}`)
    ctx.response.body = `<h1>Welcome ${name}!`
})


// 在路由之前添加解析
app.use(bodyParser())
app.use(router.routes())

app.listen(8080)
console.log('koa server start at port 8080')


