/**
 * 提交表单数据的处理
 */
const Koa = require('koa')
const fs = require('fs')
const router = require('koa-router')()
const bodyParser = require('koa-bodyparser')
const nunjucks = require('./nunjucks/nunjucks')('views')

const app = new Koa()

// 日志处理
app.use(async (ctx, next) => {
    const start = new Date()
    try {
        console.log(`request ${ctx.method} ${ctx.request.url} from ${ctx.ip}`)
        await next();
    } catch (err) {
        console.log(`error : ${err}`)
    } finally {
        console.log(`request end, duration : ${new Date() - start} ms`)
    }
})

// 向ctx 绑定render 方法
app.use(async (ctx, next) => {
    ctx.render = function (view, model) {
        // nunjucks render
        ctx.response.body = nunjucks.render(view, Object.assign({}, ctx.state || {}, model || {}))
        // 设置Content-Type
        ctx.response.type = 'text/html'
    }
    await next()
})

// 解析表单数据
app.use(bodyParser())

// 路由 controller
const controllerDir = __dirname + '/controllers/'
fs.readdirSync(controllerDir)
    .filter(f => f.endsWith('.js'))
    .forEach(f => {
        let { url, middleware, method = 'get' } = require(controllerDir + f)
        method = method.toLowerCase()
        if (method === 'get') {
            router.get(url, middleware)
        } else if (method === 'post') {
            router.post(url, middleware)
        } else {
            throw 'unsupported method : ' + method
        }
    })
app.use(router.routes())

app.listen(8080)
console.log('koa server start at port 8080')
